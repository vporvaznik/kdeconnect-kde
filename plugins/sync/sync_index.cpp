/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sync_index.h"
#include "sync_log.h"
#include "common.h"
#include <QString>
#include <QMap>
#include <QPair>
#include <QtCore/QDir>
#include <QLoggingCategory>
#include <QtCore/QDateTime>
#include <QtCore/QJsonDocument>
#include <QStandardPaths>
#include <QtCore/QJsonArray>
#include <QtCore/QCryptographicHash>

SyncPlugin::SyncIndex *SyncPlugin::SyncIndex::s_instance = NULL;

Q_LOGGING_CATEGORY(KDECONNECT_PLUGIN_SYNC_INDEX, "kdeconnect.plugin.sync")

SyncPlugin::SyncIndex::SyncIndex(const QString &deviceId,
                                 const QString &syncIndexFsPath)
{
  m_deviceId = deviceId;
  m_syncIndexFsPath = syncIndexFsPath;
  createIfNotExisting(m_syncIndexFsPath, m_deviceId);
  init(m_syncIndexFsPath, m_deviceId);
}

void SyncPlugin::SyncIndex::init(const QString &fsPath, const QString &deviceId)
{
  QByteArray indexData;
  QFile indexFile;
  indexFile.setFileName(fsPath);
  if (!indexFile.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_INDEX)
      << SyncPlugin::INDEX_READ_OPEN_ERROR.arg(indexFile.fileName(),
                                               indexFile.errorString());
  }
  else
  {
    indexData = indexFile.readAll();
    m_data = QJsonDocument::fromJson(indexData).object();
  }
}

void
SyncPlugin::SyncIndex::createIfNotExisting(const QString &fsPath,
                                           const QString &deviceId)
{
  QFileInfo indexFileInfo(fsPath);

  QDir root("");
  if (!root.exists(fsPath))
  {
    // Create parent dir if not exists
    if (!root.exists(indexFileInfo.dir().absolutePath()))
    {
      root.mkpath(indexFileInfo.dir().absolutePath());
      qCDebug(KDECONNECT_PLUGIN_SYNC_INDEX) << "Creating dir "
                                            << indexFileInfo.dir().absolutePath();
    }

    writeDefaultData(fsPath);

    qCDebug(KDECONNECT_PLUGIN_SYNC_INDEX)
      << "Creating new sync index for the device " << deviceId;
  }
  else
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_INDEX)
      << SyncPlugin::INDEX_ALREADY_EXISTS.arg(deviceId);
  }
}

void SyncPlugin::SyncIndex::writeToFs(const QString &fsPath,
                                      const QByteArray &data)
{
  QFile indexFile;
  indexFile.setFileName(fsPath);
  if (!indexFile.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_INDEX)
      << SyncPlugin::INDEX_WRITE_OPEN_ERROR.arg(indexFile.fileName(),
                                                indexFile.errorString());
  }
  else
  {
    indexFile.write(data);
  }
  indexFile.close();
}

void SyncPlugin::SyncIndex::writeDefaultData(const QString &fsPath)
{
  QJsonDocument defaultDoc;
  QJsonObject defaultContent;
  defaultContent["lastUpdateTime"] = QDateTime::currentMSecsSinceEpoch();
  defaultContent["data"] = QJsonObject();
  defaultDoc.setObject(defaultContent);
  m_data = defaultContent;
  writeToFs(fsPath, defaultDoc.toJson());
}

SyncPlugin::SyncIndex &SyncPlugin::SyncIndex::instance()
{
  if (s_instance == NULL)
  {
    throw std::logic_error(NO_INSTANCE_AVAILABLE_ERROR.toStdString());
  }
  return *s_instance;
}

SyncPlugin::SyncIndex &SyncPlugin::SyncIndex::instance(const QString &deviceId,
                                                       const QString &syncIndexFsPath)
{
  if (s_instance == NULL)
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_INDEX)
      << "New sync index instance from path " << syncIndexFsPath
      << " will be created.";
    s_instance = new SyncPlugin::SyncIndex(deviceId, syncIndexFsPath);
  }
  return *s_instance;
}

void SyncPlugin::SyncIndex::destroyInstance()
{
  if (s_instance != NULL)
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_INDEX) << "Sync index instance for device "
                                          << s_instance->m_deviceId
                                          << " will be removed";
    delete s_instance;
    s_instance = NULL;
  }
}

void SyncPlugin::SyncIndex::update(QPair<QString, QString> record)
{
  QJsonDocument defaultDoc;
  QJsonObject index = m_data["data"].toObject();
  index[record.first] = record.second;
  m_data["data"] = index;
  m_data["lastUpdateTime"] = QDateTime::currentMSecsSinceEpoch();
  defaultDoc.setObject(m_data);
  writeToFs(m_syncIndexFsPath, defaultDoc.toJson());
}

QString
SyncPlugin::SyncIndex::search(const QString &localRemotePairHash)
{
  QString localRemotePairHashRecord = "";
  QJsonObject indexData = m_data["data"].toObject();
  QJsonObject::iterator hashRecord = indexData.find(localRemotePairHash);
  if (hashRecord != indexData.end())
  {
    localRemotePairHashRecord = hashRecord.value().toString();
  }
  return localRemotePairHashRecord;
}

QMap<QString, QString>
SyncPlugin::SyncIndex::getAll()
{
  QJsonObject indexData = m_data["data"].toObject();
  QMap<QString, QString> all;
  for (QJsonObject::const_iterator it = indexData.constBegin();
       it != indexData.constEnd(); ++it)
  {
    all.insert(it.key(), it.value().toString());
  }
  return all;
}
