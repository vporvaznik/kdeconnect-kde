/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QJsonDocument>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QJsonArray>
#include <QtCore/QLoggingCategory>
#include "sync_executor.h"
#include "common.h"
#include "sync_log.h"
#include "sync_index.h"

Q_LOGGING_CATEGORY(KDECONNECT_PLUGIN_SYNC_EXECUTOR, "kdeconnect.plugin.sync");

SyncPlugin::SyncExecutor::SyncExecutor(Plugin *parent)
{
  m_parent = parent;
}

const SyncPlugin::SyncResult &SyncPlugin::SyncExecutor::executionResult()
{
  return m_executionResult;
}

// TODO make this async
void SyncPlugin::SyncExecutor::run()
{
  QJsonObject jsonEntriesObject = QJsonDocument::fromJson(
      m_parent->config()->get<QByteArray>(QStringLiteral("syncEntries"),
                                          "{}")).object();
  qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "jsonEntriesObject"
                                           << jsonEntriesObject;
  QJsonArray syncEntries = getSyncEntries(jsonEntriesObject);
  qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "syncEntries" << syncEntries;
  for (const QJsonValue &value: syncEntries)
  {
    m_executionResult += synchronizeEntry(value.toObject(), true);
  }
}

QList<QJsonObject>
SyncPlugin::SyncExecutor::scanDirForEntries(const QString &localPath,
                                            const QString &remotePath,
                                            const QString &postSyncScript)
{
  QList<QString> remotePathEntries = Util::scanDir(remotePath);
  QList<QString> localPathEntries = Util::scanDir(localPath);
  QList<QString> relativeMergedPathEntries;

  for (QString localPathEntry : localPathEntries)
  {
    relativeMergedPathEntries.append(
        localPathEntry.remove(0, localPath.length()));
  }
  for (QString remotePathEntry : remotePathEntries)
  {
    QString relativeRemotePathEntry(
        remotePathEntry.remove(0, remotePath.length()));
    if (!relativeMergedPathEntries.contains(relativeRemotePathEntry))
    {
      qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "addding remotePathEntry"
                                               << relativeRemotePathEntry;
      relativeMergedPathEntries.append(relativeRemotePathEntry);
    }
  }

  QList<QJsonObject> entryList;
  for (QString relativeMergedPathEntry : relativeMergedPathEntries)
  {
    QJsonObject entry;
    entry["localPath"] = localPath + relativeMergedPathEntry;
    entry["remotePath"] = remotePath + relativeMergedPathEntry;
    entry["postSyncScript"] = postSyncScript;
    entry["createParentDirs"] = true;
    entryList.append(entry);
  }
  return entryList;
}

QJsonArray
SyncPlugin::SyncExecutor::getSyncEntries(const QJsonObject &jsonEntriesObject)
{
  const QStringList keys = jsonEntriesObject.keys();
  QJsonArray syncEntries;
  for (const QString &key : keys)
  {
    const QJsonObject entry = jsonEntriesObject[key].toObject();
    syncEntries.append(QJsonValue(entry));
  }
  return syncEntries;
}

bool SyncPlugin::SyncExecutor::executePostSyncScript(QString postSyncScript,
                                                     int operationType,
                                                     QString localFilePath,
                                                     QString remoteFilePath)
{
  bool executeResult = true;
  QString command = SyncPlugin::EXEC_BASH_SCRIPT.arg(postSyncScript,
                                                     QString::number(
                                                         operationType),
                                                     localFilePath,
                                                     remoteFilePath);
  SyncLog::instance().logDebug("Executing command: " + command);
  QString stdout, stderr;
  int exitCode = SyncPlugin::Util::runCommand(command, stdout,
                                              stderr);
  if (exitCode != 0)
  {
    SyncLog::instance().logError(SyncPlugin::EXEC_ERROR.arg(exitCode));
    SyncLog::instance().logError(stderr);
    executeResult = false;
  }
  else
  {
    SyncLog::instance().logInfo(stdout);
  }

  return executeResult;
}

SyncPlugin::SyncResult SyncPlugin::SyncExecutor::synchronizeEntry(
    const QJsonObject &entry,
    bool remotePathIsRelative)
{
  bool errorOccured = false;
  SyncPlugin::SyncResult syncResult;
  QString localPath = entry.value("localPath").toString();
  QString remotePath = entry.value("remotePath").toString();
  if (remotePathIsRelative)
  {
    remotePath = SyncPlugin::SSHFS_MOUNT_ROOT.arg(
        m_parent->remoteSystemMountPoint(),
        remotePath);
  }
  QString postSyncScript = entry.value("postSyncScript").toString();
  SyncOperation *operation = NULL;

  QFileInfo localPathInfo(localPath);
  QFileInfo remotePathInfo(remotePath);
  if (entry.contains("createParentDirs") &&
      entry.value("createParentDirs").toBool())
  {
    if (!localPathInfo.exists() && !localPathInfo.dir().exists())
    {
      qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
        << "Parent directory not existing, creating one "
        << localPathInfo.dir().absolutePath();
      QDir root("");
      root.mkpath(localPathInfo.dir().absolutePath());
    }
    else if (!remotePathInfo.exists() && !remotePathInfo.dir().exists())
    {
      qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
        << "Parent directory not existing, creating one "
        << remotePathInfo.dir().absolutePath();
      QDir root("");
      root.mkpath(remotePathInfo.dir().absolutePath());
    }
  }

  if (localPathInfo.exists() && remotePathInfo.exists())
  {
    if (localPathInfo.isDir() && !remotePathInfo.isDir())
    {
      SyncLog::instance().logError(
          SyncPlugin::INCONSISTENT_PATHS_ERROR.arg(localPath,
                                                   remotePath));
      errorOccured = true;
    }
    else if (!localPathInfo.isDir() && remotePathInfo.isDir())
    {
      SyncLog::instance().logError(
          SyncPlugin::INCONSISTENT_PATHS_ERROR.arg(remotePath,
                                                   localPath));
      errorOccured = true;
    }
    else if (localPathInfo.isDir() && remotePathInfo.isDir())
    {
      QList<QJsonObject> dirEntries = scanDirForEntries(localPath,
                                                        remotePath,
                                                        postSyncScript);
      for (QJsonObject dirEntry : dirEntries)
      {
        qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "Synchronizing nested entry"
                                                 << dirEntry;
        syncResult += synchronizeEntry(dirEntry, false);
      }
    }
    else
    {
      operation = new SyncOperation(
          determineSyncOperation(
              localPath, remotePath));
    }
  }
  else if (localPathInfo.exists() && !remotePathInfo.exists() &&
           localPathInfo.isFile() && remotePathInfo.dir().exists())
  {
    // If synchronized file is file and it does not exist on one end, but
    // its parent directory does then
    operation = new SyncOperation(determineSyncOperation(
        localPath, remotePath));
  }
  else if (!localPathInfo.exists() && remotePathInfo.exists() &&
           remotePathInfo.isFile() && localPathInfo.dir().exists())
  {
    operation = new SyncOperation(determineSyncOperation(
        localPath, remotePath));
  }
  else
  {
    SyncLog::instance().logError(
        SyncPlugin::WRONG_PATHS_ERROR.arg(localPath, remotePath));
    errorOccured = true;
  }

  if (operation != NULL)
  {
    if (!performSyncOperation(*operation, localPath,
                              remotePath,
                              postSyncScript))
    {
      errorOccured = true;
    }
    syncResult.addSynchronizedFile();
  }
  if ((operation != NULL) && (postSyncScript != "") &&
      ((operation->getType() == SyncOperation::FROM_REMOTE) ||
       (operation->getType() == SyncOperation::FROM_LOCAL)))
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "Executing post install script "
                                             << postSyncScript;
    if (!executePostSyncScript(postSyncScript, operation->getType(), localPath,
                               remotePath))
    {
      errorOccured = true;
    }
  }

  if (errorOccured)
  {
    syncResult.addError();
  }

  return syncResult;
}

bool
SyncPlugin::SyncExecutor::performSyncOperation(const SyncOperation &operation,
                                               const QString &localFilePath,
                                               const QString &remoteFilePath,
                                               const QString &postSyncScript)
{
  qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "Processing sync path"
                                           << localFilePath
                                           << "and" << remoteFilePath;
  bool operationResult = true;

  if (operation.getType() == SyncOperation::UNKNOWN)
  {
    SyncLog::instance().logError(
        SyncPlugin::SYNC_OPERATION_UNKNOWN.arg(localFilePath,
                                               remoteFilePath));
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
      << SyncPlugin::SYNC_OPERATION_UNKNOWN.arg(localFilePath,
                                                remoteFilePath);
    operationResult = false;
  }
  else if (operation.getType() == SyncOperation::NONE)
  {
    SyncLog::instance().logInfo(
        SyncPlugin::NO_SYNC.arg(localFilePath, remoteFilePath));
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
      << SyncPlugin::NO_SYNC.arg(localFilePath, remoteFilePath);
  }
  else if (operation.getType() == SyncOperation::CONFLICT)
  {
    SyncLog::instance().logError(
        SyncPlugin::SYNC_OPERATION_CONFLICT.arg(localFilePath,
                                                remoteFilePath));
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
      << SyncPlugin::SYNC_OPERATION_CONFLICT.arg(localFilePath,
                                                 remoteFilePath);
    operationResult = false;
  }
  else if (operation.getType() == SyncOperation::FROM_LOCAL)
  {
    SyncLog::instance().logInfo(
        SyncPlugin::SYNC_OPERATION_FROM_LOCAL.arg(localFilePath,
                                                  remoteFilePath));
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
      << SyncPlugin::SYNC_OPERATION_FROM_LOCAL.arg(localFilePath,
                                                   remoteFilePath);
    if (!copyFile(localFilePath, remoteFilePath))
    {
      operationResult = false;
    }
    SyncIndex::instance().update(operation.getHashRecord());
  }
  else if (operation.getType() == SyncOperation::FROM_REMOTE)
  {
    SyncLog::instance().logInfo(
        SyncPlugin::SYNC_OPERATION_FROM_REMOTE.arg(remoteFilePath,
                                                   localFilePath));
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
      << SyncPlugin::SYNC_OPERATION_FROM_REMOTE.arg(remoteFilePath,
                                                    localFilePath);
    if (!copyFile(remoteFilePath, localFilePath))
    {
      operationResult = false;
    }
    SyncIndex::instance().update(operation.getHashRecord());
  }

  return operationResult;
}

bool
SyncPlugin::SyncExecutor::copyFile(QString sourceFilePath, QString destFilePath)
{
  bool copyResult = true;
  QFile sourceFile(sourceFilePath);
  QFile destFile(destFilePath);
  QFileInfo sourceFileInfo(sourceFilePath);

  destFile.remove();
  bool result = sourceFile.copy(destFilePath);

  destFile.setPermissions(sourceFile.permissions());

  if (result)
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "File copy from '"
                                             << sourceFilePath
                                             << " to " << destFilePath
                                             << " successful";
  }
  else
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR) << "File copy from '"
                                             << sourceFilePath
                                             << " to " << destFilePath
                                             << " failed";
    copyResult = false;
  }

  // TODO it seems that it is not working (not by using shell nor utime)
  QString command = SyncPlugin::MTIME_MOD_CMD.arg(
      sourceFileInfo.lastModified().toString(
          SyncPlugin::MTIME_DATETIME_FORMAT), destFilePath);
  QString stdout, stderr;
  int exitCode = SyncPlugin::Util::runCommand(command, stdout,
                                              stderr);
  if (exitCode != 0)
  {
    SyncLog::instance().logError(SyncPlugin::EXEC_ERROR.arg(exitCode));
    SyncLog::instance().logError(stderr);
    copyResult = false;
  }

  return copyResult;
}

SyncPlugin::SyncOperation
SyncPlugin::SyncExecutor::determineSyncOperation(
    const QString &localPath,
    const QString &remotePath)
{
  // TODO improve structure (if / else), possibly introduce uni-directional sync
  QString localHash = Util::convertToHash(localPath);
  QString remoteHash = Util::convertToHash(remotePath);
  QString currentLocalRemotePairHash = localHash + remoteHash;
  QString existingLocalRemotePairHash = SyncIndex::instance().search(
      currentLocalRemotePairHash);
  QFileInfo localPathInfo = QFileInfo(localPath);
  QFileInfo remotePathInfo = QFileInfo(remotePath);

  SyncOperation operation;
  if (!localPathInfo.exists() && !remotePathInfo.exists())
  {
    // Nothing exists, no operation
  }
  else if (!localPathInfo.exists() && remotePathInfo.exists())
  {
    // Remote path exist - operation will be FROM_REMOTE
    operation.setType(SyncOperation::FROM_REMOTE);
    operation.setHashRecord(currentLocalRemotePairHash,
                            Util::convertToHash(
                                Util::getFileContent(remotePath)));
  }
  else if (localPathInfo.exists() && !remotePathInfo.exists())
  {
    // Local path exist - operation will be FROM_LOCAL
    operation.setType(SyncOperation::FROM_LOCAL);
    operation.setHashRecord(currentLocalRemotePairHash,
                            Util::convertToHash(
                                Util::getFileContent(localPath)));
  }
  else if (localPathInfo.exists() && remotePathInfo.exists())
  {
    QString localContentHash = Util::convertToHash(
        Util::getFileContent(localPath));
    QString remoteContentHash = Util::convertToHash(
        Util::getFileContent(remotePath));
    // Both files / directories exist
    if (existingLocalRemotePairHash == "")
    {
      SyncLog::instance().logInfo(
          SyncPlugin::SYNC_DETERMINATION_BY_MTIME.arg(localPath,
                                                      remotePath));
      qCDebug(KDECONNECT_PLUGIN_SYNC_EXECUTOR)
        << SyncPlugin::SYNC_DETERMINATION_BY_MTIME.arg(localPath,
                                                       remotePath);
      if (localPathInfo.lastModified().toMSecsSinceEpoch() ==
          remotePathInfo.lastModified().toMSecsSinceEpoch())
      {
        operation.setType(SyncOperation::NONE);
      }
      else if (localPathInfo.lastModified().toMSecsSinceEpoch() >=
               remotePathInfo.lastModified().toMSecsSinceEpoch())
      {
        operation.setType(SyncOperation::FROM_LOCAL);
        operation.setHashRecord(currentLocalRemotePairHash,
                                localContentHash);
      }
      else
      {
        operation.setType(SyncOperation::FROM_REMOTE);
        operation.setHashRecord(currentLocalRemotePairHash,
                                remoteContentHash);
      }
    }
    else
    {
      if (remoteContentHash == localContentHash)
      {
        operation.setType(SyncOperation::NONE);
      }
      else if ((existingLocalRemotePairHash == remoteContentHash) &&
               (existingLocalRemotePairHash != localContentHash))
      {
        operation.setType(SyncOperation::FROM_LOCAL);
        operation.setHashRecord(currentLocalRemotePairHash, localContentHash);
      }
      else if ((existingLocalRemotePairHash != remoteContentHash) &&
               (existingLocalRemotePairHash == localContentHash))
      {
        operation.setType(SyncOperation::FROM_REMOTE);
        operation.setHashRecord(currentLocalRemotePairHash, remoteContentHash);
      }
      else if ((existingLocalRemotePairHash != remoteContentHash) &&
               (existingLocalRemotePairHash != localContentHash))
      {
        operation.setType(SyncOperation::CONFLICT);
      }
    }
  }

  return operation;
}
