/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include <QLoggingCategory>
#include <QtCore/QCryptographicHash>
#include <QtGui/QtGui>

Q_LOGGING_CATEGORY(KDECONNECT_PLUGIN_SYNC_COMMON, "kdeconnect.plugin.sync")

int
SyncPlugin::Util::runCommand(const QString &commandToRun, QString &stdout,
                             QString &stderr)
{
  qCDebug(KDECONNECT_PLUGIN_SYNC_COMMON) << "Executing command: "
                                         << commandToRun;

  QProcess process;
  process.start(commandToRun);
  process.waitForFinished(-1);

  stdout = process.readAllStandardOutput().trimmed();
  stderr = process.readAllStandardError().trimmed();

  return process.exitCode();
}

QString SyncPlugin::Util::getFileContent(const QString &filePath)
{
  QString fileContent = "";
  QFile file(filePath);
  if (file.open(QIODevice::ReadOnly))
  {
    fileContent = file.readAll();
  }
  return fileContent;
}

QString SyncPlugin::Util::convertToHash(const QString &toConvert)
{

  QString hash = "";
  QByteArray hashData = QCryptographicHash::hash(toConvert.toLocal8Bit(),
                                                 QCryptographicHash::Sha1);
  hash = hashData.toHex();
  return hash;
}

QList<QString> SyncPlugin::Util::scanDir(const QString &rootElement)
{
  QList<QString> files;
  QFileInfo rootElementInfo(rootElement);
  if (rootElementInfo.exists() && rootElementInfo.isDir())
  {
    QDir rootDir(rootElement);
    QList<QFileInfo> allEntries = rootDir.entryInfoList(
        QDir::AllEntries | QDir::NoDotAndDotDot);
    qCDebug(KDECONNECT_PLUGIN_SYNC_COMMON) << "Scanning " << rootElement
                                           << " size " << allEntries.size();
    for (QFileInfo &entry : allEntries)
    {
      if (entry.isDir())
      {
        qCDebug(KDECONNECT_PLUGIN_SYNC_COMMON) << "Dir "
                                               << entry.absoluteFilePath();
        files.append(scanDir(entry.absoluteFilePath()));
      }
      else if (entry.isFile())
      {
        qCDebug(KDECONNECT_PLUGIN_SYNC_COMMON) << "File "
                                               << entry.absoluteFilePath();
        files.append(entry.absoluteFilePath());
      }
    }
  }
  else
  {
    throw std::runtime_error(
        QString("Can not scan, path is not a directory! (%1)").arg(
            rootElement).toStdString());
  }

  return files;
}
