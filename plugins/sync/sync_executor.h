/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDECONNECT_SYNC_EXECUTOR_H
#define KDECONNECT_SYNC_EXECUTOR_H

#include <QObject>
#include "sync_operation.h"
#include "sync_result.h"
#include "plugin.h"

namespace SyncPlugin
{
  class SyncExecutor : public QObject
  {
  Q_OBJECT
  private:
    SyncResult m_executionResult;
    Plugin *m_parent;

  public:
    SyncExecutor(Plugin *pluginInstance);

    void run();

    const SyncResult &executionResult();

    SyncResult synchronizeEntry(const QJsonObject &entry,
                                bool remotePathIsRelative);

    bool performSyncOperation(const SyncOperation &operation,
                              const QString &localFilePath,
                              const QString &remoteFilePath,
                              const QString &postSyncScript);

    SyncPlugin::SyncOperation
    determineSyncOperation(const QString &localPath, const QString &remotePath);

    bool copyFile(QString sourceFilePath, QString destFilePath);

    QJsonArray getSyncEntries(const QJsonObject &jsonEntriesObject);

    bool
    executePostSyncScript(QString postSyncScript,
                          int operationType, QString localFilePath,
                          QString remoteFilePath);

    QList<QJsonObject>
    scanDirForEntries(const QString &localPath, const QString &remotePath,
                      const QString &postSyncScript);
  };
}

#endif //KDECONNECT_SYNC_EXECUTOR_H
