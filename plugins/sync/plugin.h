/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOOKSPLUGIN_H
#define HOOKSPLUGIN_H

#include <QObject>

#include <core/kdeconnectplugin.h>
#include <QtDBus/QDBusPendingCallWatcher>
#include <QtDBus/QDBusPendingCallWatcher>
#include <QString>

namespace SyncPlugin
{

  class Q_DECL_EXPORT Plugin
      : public KdeConnectPlugin
  {
  Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.kdeconnect.device.sync")

  private:
    QString m_remoteSystemMountPoint;

    /**
     * Waits max amount of time till device's remote root is available
     * @param deviceId of device that was just mounted
     * @param timeout max wait time (in seconds)
     */
    bool waitForRemoteRoot(QString deviceId, int timeout);

    /**
     * Performs actual file synchronization and sends notification
     * (KNotification) about its completion
     */
    void synchronize();

    /**
     *
     * @return true if remote root is available, else false
     */
    bool isRemoteSystemRootAvailable(const QString &deviceId);

    QString findMountFolderForDevice(const QString &deviceId);

  public:
    virtual QString remoteSystemMountPoint();

    /**
     * Constructs sync plugin
     * TODO make this depended on SFTP pluging, if that is not available then
     * it can not be available
     * @param parent
     * @param args
     */
    explicit Plugin(QObject
                    *parent,
                    const QVariantList &args);

    ~Plugin() override;

    /**
     * TODO hook it up to own packet type (needs to modify android application)
     * @param np
     * @return
     */
    bool receivePacket(const NetworkPacket &np) override;

    void connected() override
    {}

    QString dbusPath() const override;

  public Q_SLOTS:

    /**
     * Called when SFTP plugin sends "mounted" signal to D-BUS, only then
     * a remote root can be accessed
     */
    void sftpMounted();
  };
}

#endif
