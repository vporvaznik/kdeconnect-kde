/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDECONNECT_SYNC_LOG_H
#define KDECONNECT_SYNC_LOG_H

#include <QString>
#include <QtCore/QDateTime>
#include <QtCore/QObject>
#include <QtCore/QFile>

namespace SyncPlugin
{
  class SyncLog : QObject
  {
  Q_OBJECT
  public:
    enum LogLevel
    {
      DEBUG, INFO, ERROR
    };

    SyncLog(const QString &deviceId, LogLevel publicLogLevel);

    ~SyncLog();

    static SyncLog &instance();

    static SyncLog &instance(const QString &deviceId);

    static void destroyInstance();

    void setPublicLogLevel(LogLevel publicLogLevel);

    void logError(const QString &message);

    void logInfo(const QString &message);

    void logDebug(const QString &message);

    const QString getLogFilePath();

    const QString getLogLevelAsString(LogLevel logLevel);

  private:
    void logMessage(const QString &message, LogLevel level);

    static SyncLog *s_instance;
    QFile *m_logFile;
    LogLevel m_publicLogLevel;
    QString m_logFilePath;
    QString m_deviceId;
  };
}

#endif //KDECONNECT_SYNC_LOG_H
