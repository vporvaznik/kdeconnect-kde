/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDECONNECT_SYNC_OPERATION_H
#define KDECONNECT_SYNC_OPERATION_H

#include <QtCore/QString>
#include <QtCore/QPair>

namespace SyncPlugin
{
  class SyncOperation
  {
  public:
    enum SyncOperationType
    {
      UNKNOWN, NONE, CONFLICT, FROM_REMOTE, FROM_LOCAL
    };

    SyncOperation();

    SyncOperation(const SyncOperation &other);

    SyncOperationType getType() const;

    QPair<QString, QString> getHashRecord() const;

    void setType(SyncOperationType newType);

    void setHashRecord(const QString &newHashRecordKey,
                       const QString &newHashRecordValue);

  private:
    SyncOperationType m_type;
    QString m_indexRecordKey;
    QString m_indexRecordValue;
  };
}

#endif //KDECONNECT_SYNC_OPERATION_H
