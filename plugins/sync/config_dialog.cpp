/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config_dialog.h"

#include <KPluginFactory>
#include <QTableView>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QMenu>
#include <QStandardItemModel>
#include <QDebug>
#include <QUuid>
#include <QJsonDocument>
#include <QJsonArray>
#include <KLocalizedString>
#include <QLoggingCategory>
#include <core/dbushelper.h>

K_PLUGIN_FACTORY(SyncConfigFactory, registerPlugin<SyncPlugin::ConfigDialog>();)

Q_LOGGING_CATEGORY(KDECONNECT_PLUGIN_SYNC, "kdeconnect.plugin.sync")

SyncPlugin::ConfigDialog::ConfigDialog(QWidget *parent,
                                       const QVariantList &args)
    : KdeConnectPluginKcm(parent, args,
                          QStringLiteral("kdeconnect_sync_config"))
{
  QTableView *table = new QTableView(this);
  table->horizontalHeader()->setStretchLastSection(true);
  table->verticalHeader()->setVisible(false);
  QVBoxLayout *layout = new QVBoxLayout(this);
  layout->addWidget(table);
  QPushButton *button;
  button = new QPushButton(QIcon::fromTheme(QStringLiteral("list-remove")),
                           i18n("Remove sync entry"), this);
  layout->addWidget(button);

  setLayout(layout);

  m_entriesModel = new QStandardItemModel(this);
  table->setModel(m_entriesModel);

  m_entriesModel->setHorizontalHeaderLabels(
      QStringList() << i18n("Remote path") << i18n("Local path")
                    << i18n("Post sync script"));
}

SyncPlugin::ConfigDialog::~ConfigDialog()
{
}

void SyncPlugin::ConfigDialog::defaults()
{
  KCModule::defaults();
  m_entriesModel->clear();
  Q_EMIT changed(true);
}

void SyncPlugin::ConfigDialog::load()
{
  KCModule::load();

  QJsonDocument jsonDocument = QJsonDocument::fromJson(
      config()->get<QByteArray>(QStringLiteral("syncEntries"), "{}"));
  QJsonObject jsonConfig = jsonDocument.object();
  const QStringList keys = jsonConfig.keys();
  for (const QString &key : keys)
  {
    const QJsonObject entry = jsonConfig[key].toObject();
    const QString remotePath = entry[QStringLiteral("remotePath")].toString();
    const QString localPath = entry[QStringLiteral("localPath")].toString();
    const QString postSyncScript = entry[QStringLiteral(
        "postSyncScript")].toString();

    QStandardItem *newRemotePath = new QStandardItem(remotePath);
    newRemotePath->setEditable(true);
    QStandardItem *newLocalPath = new QStandardItem(localPath);
    newLocalPath->setEditable(true);
    QStandardItem *newPostSyncScript = new QStandardItem(postSyncScript);
    newPostSyncScript->setEditable(true);

    m_entriesModel->appendRow(
        QList<QStandardItem *>() << newRemotePath << newLocalPath
                                 << newPostSyncScript);
  }

  m_entriesModel->sort(0);

  insertEmptyRow();
  connect(m_entriesModel, &QAbstractItemModel::dataChanged, this,
          &ConfigDialog::onDataChanged);

  Q_EMIT changed(false);
}

void SyncPlugin::ConfigDialog::save()
{
  QJsonObject jsonConfig;
  for (int i = 0; i < m_entriesModel->rowCount(); i++)
  {
    QString key = QUuid::createUuid().toString();
    DbusHelper::filterNonExportableCharacters(key);
    const QString remotePath = m_entriesModel->item(i, 0)->text();
    const QString localPath = m_entriesModel->item(i, 1)->text();
    const QString postSyncScript = m_entriesModel->item(i, 2)->text();

    if (remotePath.isEmpty() || localPath.isEmpty())
    {
      continue;
    }

    QJsonObject entry;
    entry[QStringLiteral("remotePath")] = remotePath;
    entry[QStringLiteral("localPath")] = localPath;
    entry[QStringLiteral("postSyncScript")] = postSyncScript;
    jsonConfig[key] = entry;
  }
  QJsonDocument document;
  document.setObject(jsonConfig);
  config()->set(QStringLiteral("syncEntries"),
                document.toJson(QJsonDocument::Compact));

  KCModule::save();

  Q_EMIT changed(false);
}

void SyncPlugin::ConfigDialog::onDataChanged(const QModelIndex &topLeft,
                                             const QModelIndex &bottomRight)
{
  Q_EMIT changed(true);
  Q_UNUSED(topLeft);
  if (bottomRight.row() == m_entriesModel->rowCount() - 1)
  {
    insertEmptyRow();
  }
}

void SyncPlugin::ConfigDialog::insertRow(int i, const QString &remotePath,
                                         const QString &localPath,
                                         const QString &postSyncScript)
{
  QStandardItem *newRemotePath = new QStandardItem(remotePath);
  newRemotePath->setEditable(true);
  QStandardItem *newLocalPath = new QStandardItem(localPath);
  newLocalPath->setEditable(true);
  QStandardItem *newPostSyncScript = new QStandardItem(postSyncScript);
  newPostSyncScript->setEditable(true);

  m_entriesModel->insertRow(i, QList<QStandardItem *>() << newRemotePath
                                                        << newLocalPath
                                                        << newPostSyncScript);
}

void SyncPlugin::ConfigDialog::insertEmptyRow()
{
  insertRow(m_entriesModel->rowCount(), {}, {}, {});
}

#include "config_dialog.moc"
