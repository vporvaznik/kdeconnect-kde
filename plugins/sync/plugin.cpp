/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "sync_index.h"
#include "sync_log.h"
#include "plugin.h"
#include "sync_executor.h"

#include <KLocalizedString>
#include <KPluginFactory>

#include <QDebug>
#include <QDBusConnection>
#include <QLoggingCategory>
#include <QProcess>

#include <core/device.h>
#include <core/daemon.h>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusPendingCallWatcher>
#include <QtDBus/QDBusPendingReply>
#include <QDir>
#include <QtCore/QCryptographicHash>
#include <QtCore/QStandardPaths>
#include <QJsonDocument>
#include <QJsonArray>

K_PLUGIN_FACTORY_WITH_JSON(KdeConnectPluginFactory, "kdeconnect_sync.json",
                           registerPlugin<SyncPlugin::Plugin>();)

Q_LOGGING_CATEGORY(KDECONNECT_PLUGIN_SYNC, "kdeconnect.plugin.sync")

SyncPlugin::Plugin::Plugin(QObject *parent, const QVariantList &args)
    : KdeConnectPlugin(parent, args)
{
  QString syncIndexFsPath = SyncPlugin::INDEX_PATH.arg(
      QStandardPaths::writableLocation(QStandardPaths::HomeLocation),
      device()->id());
  SyncIndex::instance(device()->id(), syncIndexFsPath);
  SyncLog::instance(device()->id());
  SyncLog::instance().setPublicLogLevel(SyncLog::DEBUG);
}

bool SyncPlugin::Plugin::receivePacket(const NetworkPacket &np)
{
  qCDebug(KDECONNECT_PLUGIN_SYNC)
    << "Sync packed received, sync will start soon" << device()->id();

  // First let's check whether the root is already available or if not then
  // connect our 'sftpMounted' slot to the 'mounted' signal and request
  // the SFTP plugin to mount it
  if (isRemoteSystemRootAvailable(device()->id()))
  {
    synchronize();
  }
  else
  {
    qCDebug(KDECONNECT_PLUGIN_SYNC) << "Mounting device" << device()->id();

    QDBusConnection::sessionBus().connect("org.kde.kdeconnect",
                                          SyncPlugin::SFTP_PLUGIN_DBUS_OBJ_PATH.arg(
                                              device()->id()),
                                          "org.kde.kdeconnect.device.sftp",
                                          "mounted", this, SLOT(sftpMounted()));

    QDBusInterface interface("org.kde.kdeconnect",
                             SyncPlugin::SFTP_PLUGIN_DBUS_OBJ_PATH.arg(
                                 device()->id()),
                             "org.kde.kdeconnect.device.sftp");
    interface.call("mount");
  }
  return true;
}

bool SyncPlugin::Plugin::isRemoteSystemRootAvailable(const QString &deviceId)
{
  bool available = false;
  m_remoteSystemMountPoint = findMountFolderForDevice(deviceId);
  if (m_remoteSystemMountPoint != "")
  {
    QFileInfo remoteSystemRoot(
        SyncPlugin::SSHFS_MOUNT_ROOT.arg(m_remoteSystemMountPoint, ""));
    if (remoteSystemRoot.exists())
    {
      qCDebug(KDECONNECT_PLUGIN_SYNC) << "Remote root already accessible";
      available = true;
    }
  }

  return available;
}

QString SyncPlugin::Plugin::findMountFolderForDevice(const QString &deviceId)
{
  QString retValue = "";
  QString stdout, stderr;

  if ((SyncPlugin::Util::runCommand(SyncPlugin::SSHFS_FIND_MOUNT_CMD,
                                    stdout, stderr) == 0) &&
      (stdout.trimmed().length() > 0) &&
      (stderr.trimmed().length() == 0))
  {
    QJsonDocument findMntJson = QJsonDocument::fromJson(stdout.toUtf8());
    QJsonArray fountMounts = findMntJson.object().value(
        "filesystems").toArray();
    for (QJsonValueRef mount : fountMounts)
    {
      QString mountTarget = mount.toObject().value("target").toString();
      if (mountTarget.contains(deviceId))
      {
        retValue = mountTarget;
        qCDebug(KDECONNECT_PLUGIN_SYNC) << "Found mount point for device "
                                        << deviceId << "(" << mountTarget
                                        << ")";
      }
      else
      {
        qCDebug(KDECONNECT_PLUGIN_SYNC) << "Mount point for device "
                                        << deviceId << " not found";
      }
    }
  }

  return retValue;
}

void SyncPlugin::Plugin::sftpMounted()
{
  qCDebug(KDECONNECT_PLUGIN_SYNC) << "Device SSHFS mount completed";

  // Wait till remote root becomes accessible (or not)
  if (waitForRemoteRoot(device()->id(), SyncPlugin::REMOTE_ROOT_TIMEOUT))
  {
    // Perform synchronization
    synchronize();
  }
  else
  {
    SyncLog::instance().logError(
        "Sync can not start, remote root unavailable!");
    qCDebug(KDECONNECT_PLUGIN_SYNC)
      << "Sync can not start, remote root unavailable!";
  }
}

void SyncPlugin::Plugin::synchronize()
{
  QString syncStartingMsg("Sync starting...");
  SyncLog::instance().logInfo(syncStartingMsg);
  qCDebug(KDECONNECT_PLUGIN_SYNC) << syncStartingMsg;

  SyncExecutor syncExecutor(this);
  // TODO this could be also async, adding to the queue, thus saving time
  syncExecutor.run();
  SyncResult syncResult = syncExecutor.executionResult();

  SyncLog::instance().logInfo("Sync completed!");
  QString msg = SyncPlugin::SYNC_COMPLETED.arg(
      syncResult.totalFilesSynced()).arg(
      syncResult.totalErrors()).arg(
      SyncLog::instance().getLogFilePath());
  QList<QUrl> urls;
  urls.append(QUrl(SyncLog::instance().getLogFilePath()));
  Daemon::instance()->sendSimpleNotification(QStringLiteral("syncCompleted"),
                                             device()->name(),
                                             msg.toStdString().c_str(),
                                             QStringLiteral("dialog-ok"), urls);
  // TODO notify android device about sucess or failure
  qCDebug(KDECONNECT_PLUGIN_SYNC) << msg;
}

bool SyncPlugin::Plugin::waitForRemoteRoot(QString deviceId, int timeout)
{
  bool retValue = false;
  m_remoteSystemMountPoint = findMountFolderForDevice(deviceId);
  if (m_remoteSystemMountPoint != "")
  {
    retValue = true;
    QFileInfo remoteSystemRoot(
        SyncPlugin::SSHFS_MOUNT_ROOT.arg(m_remoteSystemMountPoint, ""));

    for (int i = 0; i < timeout; i++)
    {
      if (remoteSystemRoot.exists())
      {
        qCDebug(KDECONNECT_PLUGIN_SYNC)
          << "SSHFS mount ready and remote root"
          << remoteSystemRoot.absolutePath()
          << "accessible";
        break;
      }
      else
      {
        QThread::sleep(1);
        qCDebug(KDECONNECT_PLUGIN_SYNC)
          << "Waiting for remote root" << remoteSystemRoot.absolutePath()
          << "to become available ...";
      }
    }
  }

  return retValue;
}

SyncPlugin::Plugin::~Plugin()
{
  qCDebug(KDECONNECT_PLUGIN_SYNC) << "Sync plugin destructor for device"
                                  << device()->name();
  SyncIndex::destroyInstance();
  SyncLog::destroyInstance();
}

QString SyncPlugin::Plugin::dbusPath() const
{
  return SyncPlugin::SYNC_PLUGIN_DBUS_OBJ_PATH.arg(device()->id());
}

QString SyncPlugin::Plugin::remoteSystemMountPoint()
{
  return m_remoteSystemMountPoint;
}

#include "plugin.moc"

