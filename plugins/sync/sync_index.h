/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDECONNECT_SYNC_INDEX_H
#define KDECONNECT_SYNC_INDEX_H

#include <QtCore/QArgument>
#include <QString>
#include <QtCore/QPair>
#include <QJsonObject>
#include <QObject>
#include "sync_operation.h"

namespace SyncPlugin
{
  /**
   * TODO mutex access if more synchronizations will be requested
   */
  class SyncIndex : QObject
  {
  Q_OBJECT
  public:
    SyncIndex(const QString &deviceId, const QString &syncIndexFsPath);

    static SyncIndex &instance();

    static SyncIndex &
    instance(const QString &deviceId, const QString &syncIndexFsPath);

    static void destroyInstance();

    QString search(const QString &localRemotePairHash);

    QMap<QString, QString> getAll();

    void update(QPair<QString, QString> record);

  private:
    QJsonObject m_data;
    QString m_deviceId;
    QString m_syncIndexFsPath;

    static SyncIndex *s_instance;

    void init(const QString &fsPath, const QString &deviceId);

    void createIfNotExisting(const QString &fsPath,
                             const QString &deviceId);

    void writeToFs(const QString &fsPath, const QByteArray &data);

    void writeDefaultData(const QString &fsPath);
  };
}

#endif //KDECONNECT_SYNC_INDEX_H
