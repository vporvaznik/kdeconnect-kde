/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sync_result.h"

SyncPlugin::SyncResult::SyncResult() : m_totalFilesSynced(0), m_totalErrors(0)
{
}

SyncPlugin::SyncResult::SyncResult(const SyncResult &other)
    : m_totalFilesSynced(other.m_totalFilesSynced),
      m_totalErrors(other.m_totalErrors)
{
}

void SyncPlugin::SyncResult::addError()
{
  m_totalErrors += 1;
}

void SyncPlugin::SyncResult::addSynchronizedFile()
{
  m_totalFilesSynced += 1;
}

int SyncPlugin::SyncResult::totalFilesSynced() const
{
  return m_totalFilesSynced;
}

int SyncPlugin::SyncResult::totalErrors() const
{
  return m_totalErrors;
}

SyncPlugin::SyncResult &
SyncPlugin::SyncResult::operator+=(const SyncPlugin::SyncResult &other)
{
  m_totalFilesSynced += other.totalFilesSynced();
  m_totalErrors += other.totalErrors();

  return *this;
}
