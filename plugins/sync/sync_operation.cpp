/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sync_operation.h"

SyncPlugin::SyncOperation::SyncOperation() : m_type(
    SyncPlugin::SyncOperation::UNKNOWN)
{
}

SyncPlugin::SyncOperation::SyncOperationType
SyncPlugin::SyncOperation::SyncOperation::getType() const
{
  return m_type;
}

void
SyncPlugin::SyncOperation::setType(SyncOperation::SyncOperationType newType)
{
  m_type = newType;
}

QPair<QString, QString> SyncPlugin::SyncOperation::getHashRecord() const
{
  return QPair<QString, QString>(m_indexRecordKey, m_indexRecordValue);
}

void SyncPlugin::SyncOperation::setHashRecord(const QString &newHashRecordKey,
                                              const QString &newHashRecordValue)
{
  m_indexRecordKey = newHashRecordKey;
  m_indexRecordValue = newHashRecordValue;
}

SyncPlugin::SyncOperation::SyncOperation(const SyncPlugin::SyncOperation &other)
    : m_type(other.getType())
{
}
