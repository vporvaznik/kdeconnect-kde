/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sync_log.h"
#include "common.h"
#include <qdebug.h>
#include <QtCore/QFile>
#include <QtCore/QException>

SyncPlugin::SyncLog *SyncPlugin::SyncLog::s_instance = NULL;

SyncPlugin::SyncLog &SyncPlugin::SyncLog::instance()
{
  if (s_instance == NULL)
  {
    throw std::logic_error(NO_INSTANCE_AVAILABLE_ERROR.toStdString());
  }
  return *s_instance;
}

SyncPlugin::SyncLog &SyncPlugin::SyncLog::instance(const QString &deviceId)
{
  if (s_instance == NULL)
  {
    s_instance = new SyncPlugin::SyncLog(deviceId, SyncPlugin::SyncLog::INFO);
  }
  return *s_instance;
}

void SyncPlugin::SyncLog::destroyInstance()
{
  if (s_instance != NULL)
  {
    delete s_instance;
    s_instance = NULL;
  }
}

SyncPlugin::SyncLog::SyncLog(const QString &deviceId,
                             SyncPlugin::SyncLog::LogLevel publicLogLevel)
{
  m_publicLogLevel = publicLogLevel;
  m_logFilePath = LOG_FILE_PATH.arg(deviceId, QString::number(
      QDateTime::currentMSecsSinceEpoch()));
  m_logFile = new QFile(m_logFilePath);
  if (m_logFile->open(QIODevice::ReadWrite))
  {
    QString logCreatedMsg("Log created");
    logMessage(logCreatedMsg, SyncPlugin::SyncLog::INFO);
  }
}

void SyncPlugin::SyncLog::logMessage(const QString &message,
                                     SyncPlugin::SyncLog::LogLevel level)
{
  if (level >= m_publicLogLevel)
  {
    QString line("%1 [%2] %3\n");
    line = line.arg(QDateTime::currentDateTime().toString(
        SyncPlugin::LOG_DATETIME_FORMAT), getLogLevelAsString(level),
                    message);
    m_logFile->write(line.toUtf8());
    m_logFile->flush();
  }
}

SyncPlugin::SyncLog::~SyncLog()
{
  m_logFile->close();
  delete m_logFile;
}

void SyncPlugin::SyncLog::setPublicLogLevel(
    SyncPlugin::SyncLog::LogLevel publicLogLevel)
{
  m_publicLogLevel = publicLogLevel;
}

void SyncPlugin::SyncLog::logError(const QString &message)
{
  logMessage(message, SyncPlugin::SyncLog::ERROR);
}

void SyncPlugin::SyncLog::logInfo(const QString &message)
{
  logMessage(message, SyncPlugin::SyncLog::INFO);
}

void SyncPlugin::SyncLog::logDebug(const QString &message)
{
  logMessage(message, SyncPlugin::SyncLog::DEBUG);
}

const QString SyncPlugin::SyncLog::getLogLevelAsString(
    SyncPlugin::SyncLog::LogLevel logLevel)
{
  QString result = "";
  if (logLevel == SyncPlugin::SyncLog::DEBUG)
  {
    result = "DEBUG";
  }
  else if (logLevel == SyncPlugin::SyncLog::INFO)
  {
    result = "INFO";
  }
  else if (logLevel == SyncPlugin::SyncLog::ERROR)
  {
    result = "ERROR";
  }
  return result;
}

const QString SyncPlugin::SyncLog::getLogFilePath()
{
  return m_logFilePath;
}
