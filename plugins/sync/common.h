/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDECONNECT_COMMON_H
#define KDECONNECT_COMMON_H

#include <exception>
#include <QtCore/QString>
#include <QtCore/QProcess>

namespace SyncPlugin
{
  // General
  static const int REMOTE_ROOT_TIMEOUT = 30;
  static const QString SFTP_PLUGIN_DBUS_OBJ_PATH = "/modules/kdeconnect/devices/%1/sftp";
  static const QString SYNC_PLUGIN_DBUS_OBJ_PATH = "/modules/kdeconnect/devices/%1/sync";
  // TODO dependency on findmnt??? maybe it will require RPM dependency update / separate update?
  static const QString SSHFS_FIND_MOUNT_CMD = "findmnt -t fuse.sshfs --json";
  // TODO phone path changing?
  static const QString SSHFS_MOUNT_ROOT = "%1/storage/emulated/0/%2";
  static const QString MTIME_MOD_CMD = "touch -d \"%1\" \"%2\"";
  static const QString MTIME_DATETIME_FORMAT = "yyyy-MM-dd hh:mm:ss";

  // Sync log
  static const QString LOG_DATETIME_FORMAT = "dd-MM-yyyy hh:mm:ss.zzz";
  // TODO this should be configurable
  static const QString LOG_FILE_PATH = "/tmp/kdeconnect-sync-%1-%2.log";

  // Post sync script
  static const QString EXEC_BASH_SCRIPT = "bash %1 %2 %3 %4";

  // Sync index
  // TODO this should be configurable
  static const QString INDEX_PATH = "%1/.kdeconnect-sync/%2/sync_db.json";

  // Errors
  static const QString NO_INSTANCE_AVAILABLE_ERROR = "No instance available, for the first time initialization please provide 'deviceId'!";
  static const QString EXEC_ERROR = "Script execution failed, exit code %1";
  static const QString INCONSISTENT_PATHS_ERROR = "Inconsistent paths settings, '%1' is a directory but '%2' is not!";
  static const QString WRONG_PATHS_ERROR = "Wrong paths settings, '%1' nor '%2' does exist!";
  static const QString INDEX_READ_OPEN_ERROR = "Could not open sync index file (%1) for reading, error: %2";
  static const QString INDEX_WRITE_OPEN_ERROR = "Could not open sync index file (%1) for writing, error: %2";
  static const QString INDEX_ALREADY_EXISTS = "Sync index for the device %1 already exists, doing nothing";

  // Sync operation messages
  static const QString SYNC_OPERATION_CONFLICT = "Conflict in versions (%1) and (%2), both versions have changed since the last sync!";
  static const QString NO_SYNC = "No sync needed both (%1) and (%2) have same content.";
  static const QString SYNC_OPERATION_UNKNOWN = "Not sure what to do with (%1) and (%2)";
  static const QString SYNC_OPERATION_FROM_LOCAL = "Local file (%1) will be uploaded to (%2)";
  static const QString SYNC_OPERATION_FROM_REMOTE = "Remote file (%1) will be downloaded to (%2)";
  static const QString SYNC_DETERMINATION_BY_MTIME = "Files (%1) and (%2) synchronized for the first time, using mod_time as decisive parameter";
  static const QString SYNC_COMPLETED = "Synchronization completed!\n%1 files synchronized with %2 errors\n\nFor details see log at %3\n";

  class Util
  {
  public:
    /**
     * Runs a command
     * @param commandToRun
     * @param stdout returned during the process execution
     * @param stderr returned during the process execution
     * @return error if any
     */
    static int
    runCommand(const QString &commandToRun, QString &stdout, QString &stderr);

    static QString convertToHash(const QString &toConvert);

    static QString getFileContent(const QString &filePath);

    static QList<QString> scanDir(const QString &rootElement);
  };
}

#endif //KDECONNECT_COMMON_H
