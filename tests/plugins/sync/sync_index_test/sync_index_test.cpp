/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QTest>
#include <QString>
#include <QFileInfo>
#include <exception>
#include <qdebug.h>
#include "sync_index.h"
#include "common.h"

namespace SyncPlugin
{
  class SyncIndexTest : public QObject
  {
  Q_OBJECT
  private:
    SyncIndex *m_index;

  private Q_SLOTS:

    void initIndex()
    {
      QString testDeviceId("testDeviceId");
      QString testIndexPath = QDir::currentPath() +
                              "/../sync_index_test/test_resources/sync_db.json";
      m_index = &SyncIndex::instance(testDeviceId, testIndexPath);
      QVERIFY(m_index != NULL);
      QVERIFY(QFileInfo(testIndexPath).exists());
    }


    void testGetAndDestroyInstance0()
    {
      SyncIndex::destroyInstance();
      QVERIFY_EXCEPTION_THROWN(SyncIndex::instance(), std::logic_error);
    }

    void testGetAndDestroyInstance1ExistingIndex()
    {
      QString testDeviceId("testDeviceId");
      QString testIndexPath = QDir::currentPath() +
                              "/../sync_index_test/test_resources/testGetAndDestroyInstance1ExistingIndex/sync_db.json";
      SyncIndex *indexA = &SyncIndex::instance(testDeviceId, testIndexPath);
      SyncIndex *indexB = &SyncIndex::instance();
      QVERIFY(indexA == indexB);
      QVERIFY(QFileInfo(testIndexPath).exists());
      SyncIndex::destroyInstance();
      QVERIFY_EXCEPTION_THROWN(SyncIndex::instance(), std::logic_error);
    }

    void testGetAndDestroyInstance1NotExistingIndex()
    {
      QString testDeviceId("testDeviceId");
      QString testIndexPath = QDir::currentPath() +
                              "/../sync_index_test/test_resources/sync_db_that_does_not_exist.json";
      SyncIndex *indexA = &SyncIndex::instance(testDeviceId, testIndexPath);
      SyncIndex *indexB = &SyncIndex::instance();
      QVERIFY(indexA == indexB);
      QVERIFY(QFileInfo(testIndexPath).exists());
      SyncIndex::destroyInstance();
      QVERIFY_EXCEPTION_THROWN(SyncIndex::instance(), std::logic_error);
    }

    void testGetAll()
    {
      initIndex();

      QMap<QString, QString> indexAData = m_index->getAll();
      QVERIFY(indexAData["file_key1"] == "file_value1");
      QVERIFY(indexAData.size() == 1);

      destroyIndex();
    }

    void testSearch()
    {
      initIndex();

      QString search = m_index->search("file_key1");
      QVERIFY(search == "file_value1");

      destroyIndex();
    }

    void testUpdateAndPersistency()
    {
      initIndex();

      m_index->update(QPair<QString, QString>("new_key", "new_value"));
      QMap<QString, QString> indexAData = m_index->getAll();
      QVERIFY(indexAData["file_key1"] == "file_value1");
      QVERIFY(indexAData["new_key"] == "new_value");
      QVERIFY(indexAData.size() == 2);

      destroyIndex();

      initIndex();

      indexAData = m_index->getAll();
      QVERIFY(indexAData["file_key1"] == "file_value1");
      QVERIFY(indexAData["new_key"] == "new_value");
      QVERIFY(indexAData.size() == 2);

      destroyIndex();
    }

    void destroyIndex()
    {
      SyncIndex::destroyInstance();
      QVERIFY_EXCEPTION_THROWN(SyncIndex::instance(), std::logic_error);
      m_index = NULL;
    }
  };
}

QTEST_MAIN(SyncPlugin::SyncIndexTest)

#include "sync_index_test.moc"
