/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QTest>
#include <QString>
#include <qdebug.h>
#include "sync_operation.h"

namespace SyncPlugin
{
  class SyncOperationTest : public QObject
  {
  Q_OBJECT
  private Q_SLOTS:

    void testConstructor0()
    {
      SyncOperation *syncOperation = new SyncOperation();
      delete syncOperation;
    }

    void testConstructor1()
    {
      SyncOperation *syncOperationA = new SyncOperation();
      QCOMPARE(syncOperationA->getType(), SyncOperation::UNKNOWN);
      syncOperationA->setType(SyncOperation::CONFLICT);
      QCOMPARE(syncOperationA->getType(), SyncOperation::CONFLICT);
      SyncOperation *syncOperationB = new SyncOperation(*syncOperationA);
      QCOMPARE(syncOperationA->getType(), SyncOperation::CONFLICT);
      delete syncOperationA;
      delete syncOperationB;
    }

    void testSetAndGetType()
    {
      SyncOperation *syncOperation = new SyncOperation();
      QCOMPARE(syncOperation->getType(), SyncOperation::UNKNOWN);
      syncOperation->setType(SyncOperation::CONFLICT);
      QCOMPARE(syncOperation->getType(), SyncOperation::CONFLICT);
    }

    void testSetGetHashRecord()
    {
      SyncOperation *syncOperation = new SyncOperation();
      QPair<QString, QString> hashRecord = syncOperation->getHashRecord();
      QCOMPARE(hashRecord.first, "");
      QCOMPARE(hashRecord.second, "");
      syncOperation->setHashRecord(QString("KEY"), QString("VALUE"));
      hashRecord = syncOperation->getHashRecord();
      QCOMPARE(hashRecord.first, QString("KEY"));
      QCOMPARE(hashRecord.second, QString("VALUE"));
    }
  };
}

QTEST_MAIN(SyncPlugin::SyncOperationTest)

#include "sync_operation_test.moc"
