/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QTest>
#include <QString>
#include <QList>
#include <qdebug.h>
#include "common.h"

namespace SyncPlugin
{
  class UtilTest : public QObject
  {
  Q_OBJECT
  private Q_SLOTS:

    void testConvertToHash()
    {
      QString emptyString("");
      QString aString(
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id facilisis mauris. Vestibulum fermentum metus sed finibus lacinia. Ut vel libero cursus, feugiat augue quis, pretium orci. Nam suscipit ligula neque. Donec laoreet elit nisi, eget semper eros faucibus quis. In molestie dapibus massa, et condimentum lacus volutpat quis. Cras sed purus at ligula bibendum euismod. Donec egestas lacus non mi dictum, ultrices blandit dui interdum. In convallis scelerisque pretium. Ut vel augue iaculis, pellentesque massa eget, sodales urna. Fusce a massa sollicitudin, tincidunt lorem a, laoreet felis. Integer velit neque, facilisis ac sagittis in, tempor ac lorem. Morbi ornare lobortis est, non pharetra justo pellentesque at. Pellentesque pretium semper massa, tincidunt cursus diam imperdiet a. Praesent dictum velit sollicitudin, facilisis quam in, mollis eros.\n\nCras vitae interdum sapien, in varius nibh. Curabitur luctus, est sit amet interdum fringilla, eros velit congue felis, a facilisis elit tellus mattis arcu. Integer mollis a arcu vel lobortis. Morbi at justo euismod, iaculis ipsum vel, efficitur lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent eget bibendum dui. Nulla pellentesque neque eget arcu pellentesque pretium.");

      QCOMPARE(Util::convertToHash(emptyString),
               "da39a3ee5e6b4b0d3255bfef95601890afd80709");
      QCOMPARE(Util::convertToHash(aString),
               "86ef5f13d67b65855eaa591b5bede5d14108e299");
    }

    void testGetFileContent()
    {
      QString anEmptyPath("");
      QString aFileThatDoesNotExist("i_am_not_here.txt");
      QString anAbsolutePathToARealFile(
          QDir::currentPath() +
          "/../common_test/test_resources/testGetFileContent/test_file1.txt");
      QCOMPARE(Util::getFileContent(anAbsolutePathToARealFile),
               QString(
                   "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id facilisis mauris. Vestibulum fermentum metus sed finibus lacinia. Ut vel libero cursus, feugiat augue quis, pretium orci. Nam suscipit ligula neque. Donec laoreet elit nisi, eget semper eros faucibus quis. In molestie dapibus massa, et condimentum lacus volutpat quis. Cras sed purus at ligula bibendum euismod. Donec egestas lacus non mi dictum, ultrices blandit dui interdum. In convallis scelerisque pretium. Ut vel augue iaculis, pellentesque massa eget, sodales urna. Fusce a massa sollicitudin, tincidunt lorem a, laoreet felis. Integer velit neque, facilisis ac sagittis in, tempor ac lorem. Morbi ornare lobortis est, non pharetra justo pellentesque at. Pellentesque pretium semper massa, tincidunt cursus diam imperdiet a. Praesent dictum velit sollicitudin, facilisis quam in, mollis eros.\n"));
      QCOMPARE(Util::getFileContent(anEmptyPath),
               "");
      QCOMPARE(Util::getFileContent(aFileThatDoesNotExist),
               "");
    }

    void testScanDir()
    {
      // Check for the existing folder
      QList<QString> expectedList = {
          QString(
              "/data/github/kdeconnect-kde/build/common_test/test_resources/testScanDir/a_folder/a_nested folder with a space in a name/a file with a weird extension.skdj"),
          QString(
              "/data/github/kdeconnect-kde/build/common_test/test_resources/testScanDir/a_folder/a_nested folder with a space in a name/an_empty file"),
          QString(
              "/data/github/kdeconnect-kde/build/common_test/test_resources/testScanDir/test_file1.txt")

      };
      QString aDirToScan(
          QDir::currentPath() + "/../common_test/test_resources/testScanDir");
      QList<QString> dirEntries = Util::scanDir(aDirToScan);
      QCOMPARE(dirEntries.size(), expectedList.size());
      for (QString entry : dirEntries)
      {
        QVERIFY(entry.endsWith(expectedList.first()));
        expectedList.removeFirst();
      }

      // Check for an empty existing dir
      aDirToScan = QString(
          QDir::currentPath() +
          "/../common_test/test_resources/testScanDir/a_folder/an empty_folder");
      dirEntries = Util::scanDir(aDirToScan);
      QVERIFY(dirEntries.size() == 0);

      // Check for an inexistent dir
      QVERIFY_EXCEPTION_THROWN(Util::scanDir(
          QString("/a dir that definitelly does not exit")),
                               std::runtime_error);
    }

    void testRunCommand()
    {
      QString cmd("bash " +
                  QDir::currentPath() +
                  "/../common_test/test_resources/testRunCommand/success.sh");
      QString stdout, stderr;
      QCOMPARE(Util::runCommand(cmd, stdout, stderr), 0);
      QCOMPARE(stdout, "All good");
      QCOMPARE(stderr, "");

      cmd = QString("bash " +
                    QDir::currentPath() +
                    "/../common_test/test_resources/testRunCommand/failure.sh");

      QCOMPARE(Util::runCommand(cmd, stdout, stderr), 3);
      QCOMPARE(stdout, "");
      QCOMPARE(stderr, "Something wrong!");
    }
  };
}

QTEST_MAIN(SyncPlugin::UtilTest)

#include "common_test.moc"
