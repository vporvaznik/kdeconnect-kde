/**
 * Copyright 2019 Viktor Porvaznik <viktor.porvaznik@piceacode.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QTest>
#include <QString>
#include <QFileInfo>
#include <exception>
#include <QVariantList>
#include <qdebug.h>
#include "sync_executor.h"
#include "common.h"
#include "device.h"
#include "plugin.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::Return;

namespace SyncPlugin
{
  class DeviceMock : public Device
  {
  public:
    DeviceMock(QObject *parent, const QString &id) : Device(parent, id)
    {};

  };

  class PluginMock : public Plugin
  {
  public:
    PluginMock(QObject *parent, const QVariantList &args) : Plugin(parent, args)
    {};

    MOCK_METHOD0(remoteSystemMountPoint, QString());
  };

  class SyncExecutorTest : public QObject
  {
  Q_OBJECT
  private:
    PluginMock *m_pluginMock;
    DeviceMock *m_deviceMock;

  private Q_SLOTS:

    void initTestCase()
    {
      QString deviceId("id");
      m_deviceMock = new DeviceMock(NULL, deviceId);

      QVariantList list;
      list.append(QVariant::fromValue(m_deviceMock));
      list.append(QVariant(QString("pluginName")));
      QStringList stringList = {"item1", "item2"};
      list.append(QVariant::fromValue(stringList));
      list.append(QVariant(QString("iconName")));

      m_pluginMock = new PluginMock(NULL, list);
      EXPECT_CALL(*m_pluginMock, remoteSystemMountPoint()).WillRepeatedly(
          testing::Return(QString("remoteSystemMountPoint")));
    }

    void test()
    {
      SyncExecutor *executor = new SyncExecutor(m_pluginMock);
    }

    void cleanupTestCase()
    {
      delete m_pluginMock;
      delete m_deviceMock;
    }
  };
}

QTEST_MAIN(SyncPlugin::SyncExecutorTest)

#include "sync_executor_test.moc"
